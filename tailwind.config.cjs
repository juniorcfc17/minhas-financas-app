/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./src/**/*.tsx"],
    theme: {
      extend: {
        spacing: {
          '100': '27rem',
        }
      },
    },
    plugins: [],
  }