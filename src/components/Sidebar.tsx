import { Link } from 'react-router-dom';

import { RxHamburgerMenu, RxDashboard, RxFileText, RxOpenInNewWindow, RxPerson } from "react-icons/rx";

export function Sidebar() {
    return (
        <div className="h-full w-16 flex flex-col items-center bg-slate-800 transition delay-75">
            <div className="h-16 w-full flex items-center justify-end mr-10">
                <RxHamburgerMenu className="text-2xl text-sky-100 hover:text-sky-300 transition ease-in font-bold cursor-pointer" />
            </div>

            <div className="mt-8">
                <ul>
                    <li>
                        <Link to='/home'>
                            <RxDashboard className="text-2xl text-sky-100 hover:text-sky-400 cursor-pointer transition ease-in" />
                        </Link>

                    </li>
                    <li>
                        <Link to='/list'>
                            <RxFileText className="text-2xl mt-10 text-sky-100 hover:text-sky-400 cursor-pointer transition ease-in" />
                        </Link>
                    </li>
                    <li>
                        <Link to='/new'>
                            <RxOpenInNewWindow className="text-2xl mt-10 text-sky-100 hover:text-sky-400 cursor-pointer transition ease-in" />
                        </Link>
                    </li>
                    <li>
                        <Link to='/users'>
                            <RxPerson className="text-2xl mt-10 text-sky-100 hover:text-sky-400 cursor-pointer transition ease-in" />
                        </Link>
                    </li>
                </ul>
            </div>
        </div>
    )
}