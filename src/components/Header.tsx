

export function Header() {
    return (
        <header className="h-16 w-full flex items-center justify-between pl-6 pr-6 ">
            <h1 className="text-slate-900 text-xl font-semibold">Minhas Finanças</h1>
            <div className="flex items-center">
                <img src="https://github.com/adilsonjrs.png" alt="Avatar" className="mr-2 rounded-full w-8 w-8"/>
                <span className="text-sm text-slate-700">Adilson Junior</span>
            </div>
        </header>
    )
}