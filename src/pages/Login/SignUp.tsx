import { Link } from 'react-router-dom';

import { CiSaveUp1, CiLock, CiMail, CiUser, CiCircleChevLeft } from "react-icons/ci";


export function SignUp() {
    return (
        <div
            className="w-96 h-100 mt-52 pl-10 pr-10 bg-slate-900 rounded-lg border-solid border-2 border-sky-900 shadow-2xl shadow-slate-700 flex flex-col items-center">

            <CiSaveUp1 className="text-sky-700 text-5xl mt-4" />
            <h2 className="text-1xl font-normal text-slate-300        mt-2">Criar nova conta
            </h2>

            <div className="w-full">
                <input placeholder="Email *" type="email" className="mt-6 h-12 w-full pl-11 text-sm text-sky-200 rounded border-solid border-2 border-slate-800 hover:border-solid hover:border-2 hover:border-slate-700 outline-none focus:border-solid focus:border-2 focus:border-emerald-400 bg-transparent transition ease-in-out delay-100" />

                <CiMail className="text-lg -mt-8 ml-4 text-sky-400" />
            </div>

            <div className="w-full">

                <input placeholder="Nome *" type="text" className="mt-8 h-12 w-full pl-11 text-sm text-sky-200 rounded border-solid border-2 border-slate-800 hover:border-solid hover:border-2 hover:border-slate-700 outline-none focus:border-solid focus:border-2 focus:border-emerald-400 transition ease-in-out delay-100 bg-transparent" />

                <CiUser className="text-lg -mt-8 ml-4 text-sky-400" />

            </div>

            <div className="w-full">

                <input placeholder="Senha *" type="password" className="mt-8 h-12 w-full pl-11 text-sm text-sky-200 rounded border-solid border-2 border-slate-800 hover:border-solid hover:border-2 hover:border-slate-700 outline-none focus:border-solid focus:border-2 focus:border-emerald-400 transition ease-in-out delay-100 bg-transparent" />

                <CiLock className="text-lg -mt-8 ml-4 text-sky-400" />

            </div>

            <button className="h-10 mt-12 w-full bg-sky-600 rounded text-base text-sky-200 hover:bg-sky-500 transition ease-linear delay-100">
                <Link to='/'>Criar conta</Link>
            </button>

            <span className="w-full mt-6 text-xl text-sky-200 underline flex justify-start hover:text-sky-500 transition ease-linear delay75">
                <Link to='/'>
                    <CiCircleChevLeft />
                </Link>
            </span>
        </div> 
    )
}