import Modal from 'react-modal';

import { RxOpenInNewWindow } from "react-icons/rx";
import { Header } from "../../components/Header";
import { Sidebar } from "../../components/Sidebar";
import { useState } from "react";
import { NewModal } from '../../components/NewModal';


export function New() {
    const [modalIsOpen, setIsOpen] = useState(false);

    const customStyles = {
        content: {
          top: '50%',
          left: '50%',
          right: 'auto',
          bottom: 'auto',
          marginRight: '-50%',
          transform: 'translate(-50%, -50%)',
          padding: '10px 100px 20px 100px',
          boxShadow: '0px 0px 51px -20px rgba(30,41,59,1)'
        },
      };

    function openModal() {
        setIsOpen(true);
    }

    function closeModal() {
        setIsOpen(false);
    }

    return (
        <div className="h-full w-full flex bg-sky-50">
            <div className="">
                <Sidebar />
            </div>
            <div className="w-full">
                <Header />

                <div className="ml-12 mr-12 mt-6">
                    <h2 className="flex items-center text-sm text-slate-600">
                        <RxOpenInNewWindow className="text-lg mr-2" />
                        Novo Lançamento
                    </h2>

                    <main className="h-full flex pl-4 pr-4 pt-4 pb-10 mt-4">
                        <button onClick={openModal} className="shadow-2xl border hover:bg-slate-200 transition ease-in font-semibold p-5 rounded text-slate-700">Novo Lançamento</button>
                        <Modal
                            isOpen={modalIsOpen}
                            onRequestClose={closeModal}
                            contentLabel="Example Modal"
                            style={customStyles}
                        >
                        <NewModal />
                        <button className='w-full p-3 border-2 border-blue-600 bg-white text-blue-600 shadow hover:bg-blue-600 hover:text-white transition ease-in font-semibold rounded mt-10'>Salvar</button>
                        <button onClick={closeModal} className='w-full p-3 border-2 border-red-400 bg-white text-red-400 shadow hover:bg-red-400 hover:text-white transition ease-in font-semibold  rounded mt-4'>Cancelar</button>
                        
                        </Modal>
                    </main>
                </div>
            </div>
        </div>
    )
}