import { RxFileText } from "react-icons/rx";
import { Header } from "../../components/Header";
import { Sidebar } from "../../components/Sidebar";


export function Lancamentos() {
    return (
        <div className="h-full w-full flex bg-sky-50">
            <div className="">
                <Sidebar />
            </div>
            <div className="w-full">
                <Header />

                <div className="ml-12 mr-12 mt-6">
                    <h2 className="flex items-center text-sm text-slate-600">
                        <RxFileText className="text-lg mr-2" />
                        Lista de Lançamentos
                    </h2>

                    <main className="h-full pl-4 pr-4 pt-4 pb-10 mt-4">
                        <div className="w-full bg-slate-900 text-sky-200 grid grid-cols-9 gap-4 border p-4 pt-6 pb-6 rounded shadow font-semibold">
                            <div className="col-span-1">Id</div>
                            <div className="col-span-1">Descrição</div>
                            <div className="col-span-1">Mês</div>
                            <div className="col-span-1">Ano</div>
                            <div className="col-span-1">Usuário</div>
                            <div className="col-span-1">Valor</div>
                            <div className="col-span-1">Dt. Cad.</div>
                            <div className="col-span-1">Tipo</div>
                            <div className="col-span-1">Status</div>
                        </div>

                        <div className="grid grid-cols-9 mt-8 items-center gap-5 w-full border p-4 rounded shadow font-light text-sm bg-slate-200">
                            <div className="">1</div>
                            <div className="">fshaojh ofbsaofb</div>
                            <div className="">Dezembro</div>
                            <div className="">2023</div>
                            <div className="">Adilson Junior</div>
                            <div className="">200.00</div>
                            <div className="">null</div>
                            <div className="">RECEITA</div>
                            <div className="">PENDENTE</div>
                        </div>
                        <div className="grid grid-cols-9 mt-4 items-center gap-5 w-full border p-4 rounded shadow font-light text-sm">
                            <div className="">2</div>
                            <div className="">fshaojh ofbsaofb</div>
                            <div className="">Dezembro</div>
                            <div className="">2023</div>
                            <div className="">Adilson Junior</div>
                            <div className="">200.00</div>
                            <div className="">null</div>
                            <div className="">RECEITA</div>
                            <div className="">PENDENTE</div>
                        </div>
                        <div className="grid grid-cols-9 mt-4 items-center gap-5 w-full border p-4 rounded shadow font-light text-sm bg-slate-200">
                            <div className="">3</div>
                            <div className="">fshaojh ofbsaofb</div>
                            <div className="">Dezembro</div>
                            <div className="">2023</div>
                            <div className="">Adilson Junior</div>
                            <div className="">200.00</div>
                            <div className="">null</div>
                            <div className="">RECEITA</div>
                            <div className="">PENDENTE</div>
                        </div>
                        <div className="grid grid-cols-9 mt-4 items-center gap-5 w-full border p-4 rounded shadow font-light text-sm">
                            <div className="">4</div>
                            <div className="">fshaojh ofbsaofb</div>
                            <div className="">Dezembro</div>
                            <div className="">2023</div>
                            <div className="">Adilson Junior</div>
                            <div className="">200.00</div>
                            <div className="">null</div>
                            <div className="">RECEITA</div>
                            <div className="">PENDENTE</div>
                        </div>
                        <div className="grid grid-cols-9 mt-4 items-center gap-5 w-full border p-4 rounded shadow font-light text-sm bg-slate-200">
                            <div className="">5</div>
                            <div className="">fshaojh ofbsaofb</div>
                            <div className="">Dezembro</div>
                            <div className="">2023</div>
                            <div className="">Adilson Junior</div>
                            <div className="">200.00</div>
                            <div className="">null</div>
                            <div className="">RECEITA</div>
                            <div className="">PENDENTE</div>
                        </div>
                        <div className="grid grid-cols-9 mt-4 items-center gap-5 w-full border p-4 rounded shadow font-light text-sm">
                            <div className="">6</div>
                            <div className="">fshaojh ofbsaofb</div>
                            <div className="">Dezembro</div>
                            <div className="">2023</div>
                            <div className="">Adilson Junior</div>
                            <div className="">200.00</div>
                            <div className="">null</div>
                            <div className="">RECEITA</div>
                            <div className="">PENDENTE</div>
                        </div>
                    </main>
                </div>
            </div>
        </div>

    )
}