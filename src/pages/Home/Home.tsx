import { RxDashboard, RxDoubleArrowUp, RxDoubleArrowDown } from "react-icons/rx";
import { BiBarChartAlt } from "react-icons/bi";

import { Header } from "../../components/Header";
import { Sidebar } from "../../components/Sidebar";


export function Home() {
    return (
        <div className="h-full w-full flex bg-sky-50">
            <div className="">
                <Sidebar />
            </div>
            <div className="w-full">
                <Header />

                <div className="container ml-12 mr-12 mt-6">
                    <h2 className="flex items-center text-sm text-slate-600">
                        <RxDashboard className="text-lg mr-2" />
                        Dashboard
                    </h2>

                    <main className="h-full flex justify-around pl-4 pr-4 pt-4 pb-10 mt-4 rounded ">

                        <div className="h-24 w-48 flex flex-col justify-center p-2 rounded shadow shadow-slate-400">
                            <div className="flex items-center text-lg">
                                <RxDoubleArrowUp className="text-3xl text-lime-500 mr-2" />
                                 Entradas
                            </div>
                            <span className="ml-10 text-green-700">R$ 7.000,00</span>

                        </div>
                        <div className="h-24 w-48 flex flex-col justify-center p-2 rounded shadow shadow-slate-400">
                            <div className="flex items-center text-lg">
                                <RxDoubleArrowDown className="text-3xl text-red-600 mr-2" />
                                 Saídas
                            </div>
                            <span className="ml-10 text-red-600">R$ -2.000,00</span>

                        </div>
                        <div className="h-24 w-48 flex flex-col justify-center p-2 rounded shadow shadow-slate-400">
                            <div className="flex items-center text-lg">
                                <BiBarChartAlt className="text-3xl text-indigo-800 mr-2" />
                                Total Geral
                            </div>
                            <span className="ml-10 text-indigo-800">R$ 5.000,00</span>
                        </div>
                    </main>
                </div>

            </div>
        </div>
    )
}