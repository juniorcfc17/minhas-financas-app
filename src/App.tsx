import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { Login } from './pages/Login/Login'
import { SignUp } from './pages/Login/SignUp';
import { Home } from './pages/Home/Home';

import './styles/global.css'
import { Lancamentos } from './pages/Lancamentos/Lancamentos';
import { New } from './pages/New/New';

export default function App() {
  return (
    <div className='h-screen w-screen flex flex-col content-evenly bg-gradient-to-b from-slate-900 to-slate-800'>

      <div className="flex justify-center h-screen">
        <Router>
          <Routes>
            <Route path="/" element={<Login />} />
            <Route path="/signup" element={<SignUp />} />
            <Route path="/home" element={<Home />} />
            <Route path="/list" element={<Lancamentos />} />
            <Route path="/new" element={<New />} />
          </Routes>
        </Router>
      </div>

    </div >
  )
}
